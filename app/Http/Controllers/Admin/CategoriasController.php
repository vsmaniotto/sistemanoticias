<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoriasController extends Controller
{
    public function index(){
        return view('admin.categorias.index');
    }

    public function visualizar(){
        return view('admin.categorias.visualizar');
    }

    public function cadastrar(){
        return view('admin.categorias.cadastrar');
    }

    public function editar(){
        return view('admin.categorias.editar');
    }

    public function deletar(){
        echo 'Função deletar';
    }
}
