<form action="#" method="post">
    <div class="form-group row">
        <label for="nome" class="col-sm-2 col-form-label text-right font-weight-bold">Nome</label>
        <div class="col-sm-10">
            <input type="text" name="nome" id="nome" value="" class="form-control">
        </div>
    </div>
    <div class="form-group row">
        <label for="email" class="col-sm-2 col-form-label text-right font-weight-bold">Email</label>
        <div class="col-sm-10">
            <input type="email" name="email" id="email" value="" class="form-control">
        </div>
    </div>
    <div class="form-group row">
        <label for="password" class="col-sm-2 col-form-label text-right font-weight-bold">Password</label>
        <div class="col-sm-10">
            <input type="password" name="password" id="password" value="" class="form-control">
        </div>
    </div>
    <div class="form-group row">
        <label for="perfil" class="col-sm-2 col-form-label text-right font-weight-bold">Perfil</label>
        <div class="col-sm-10">
            <textarea type="text" name="perfil" id="perfil" value="" rows="5" class="form-control"></textarea>
        </div>
    </div>
    <div class="form-group row">
        <label for="foto" class="col-sm-2 col-form-label text-right font-weight-bold">Foto</label>
        <div class="col-sm-10">
            <input type="file" name="foto" id="foto" value="" class="form-control-file">
        </div>
    </div>
    <div class="form-group row">
        <label for="permissao" class="col-sm-2 col-form-label text-right font-weight-bold">Permissão</label>
        <div class="col-sm-3">
            <select name="permissao" id="permissao" value="" class="form-control">
                <option value="0">Administrador</option>
                <option value="1">Editor</option>
                <option value="2">Colaborador</option>
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label for="status" class="col-sm-2 col-form-label text-right font-weight-bold">Status</label>
        <div class="col-sm-3">
            <select name="" id="" value="" class="form-control">
                <option value="0">Inativo</option>
                <option value="1">Ativo</option>
            </select>
        </div>
    </div>

    

    <div class="form-group row">
        <div class="offset-sm-2 col-sm-10">
            <button type="submit" class="btn btn-danger">Enviar</button>
            <a href="#" class="btn btn-secondary">Cancelar</a>
        </div>
    </div>
</form>