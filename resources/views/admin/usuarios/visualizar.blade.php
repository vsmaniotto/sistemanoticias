@extends('layouts.admin')

@section('titulo', 'Área administrativa')

@section('conteudo')

    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>Visualizar Usuário</h2>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-12">
                <table class="table table-striped table-condensed">
                    <tr>
                        <th width="150">ID</th>
                        <td>1</td>
                    </tr>
                    <tr>
                        <th width="150">Nome</th>
                        <td>Pedrin</td>
                    </tr>
                    <tr>
                        <th width="150">Email</th>
                        <td>email@email.com</td>
                    </tr>
                    <tr>
                        <th width="150">Perfil</th>
                        <td>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Accusantium adipisci reprehenderit sunt repellat doloribus, ipsa neque, autem, blanditiis tempore non placeat libero porro impedit numquam ratione. Cupiditate magni nesciunt deleniti!</td>
                    </tr>
                    <tr>
                        <th width="150">Permissão</th>
                        <td>Editor</td>
                    </tr>
                    <tr>
                        <th width="150">Status</th>
                        <td>Ativo</td>
                    </tr>
                </table>
                <a href="#" class="btn btn-danger">Editar Usuário</a>
                <a href="#" class="btn btn-secondary">Cancelar</a>
            </div>
        </div>
    </div>


@endsection