@extends('layouts.admin')

@section('titulo', 'Área administrativa')

@section('conteudo')

    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>Visualizar Categoria</h2>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-12">
                <table class="table table-striped table-condensed">
                    <tr>
                        <th width="150">ID</th>
                        <td>1</td>
                    </tr>
                    <tr>
                        <th width="150">Nome</th>
                        <td>Esportes</td>
                    </tr>
                    <tr>
                        <th width="150">Status</th>
                        <td>Ativo</td>
                    </tr>
                    <tr>
                        <th width="150">Qtde Notícias</th>
                        <td>20</td>
                    </tr>
                    <tr>
                        <th width="150">Data Cadastro</th>
                        <td>20/05/2019</td>
                    </tr>
                </table>
                <a href="#" class="btn btn-danger">Editar Categoria</a>
                <a href="#" class="btn btn-secondary">Cancelar</a>
            </div>
        </div>
    </div>


@endsection