<form action="#" method="post">
    <div class="form-group row">
        <label for="nome" class="col-sm-2 col-form-label text-right font-weight-bold">Nome</label>
        <div class="col-sm-5">
            <input type="text" name="nome" id="nome" value="" class="form-control">
        </div>
    </div>
    <div class="form-group row">
        <label for="status" class="col-sm-2 col-form-label text-right font-weight-bold">Status</label>
        <div class="col-sm-3">
            <select name="status" id="status" class="form-control">
                <option value="0">Inativo</option>
                <option value="1">Ativo</option>
            </select>
        </div>
    </div>
    <div class="form-group row">
        <div class="offset-sm-2 col-sm-10">
            <button type="submit" class="btn btn-danger">Enviar</button>
            <a href="#" class="btn btn-secondary">Cancelar</a>
        </div>
    </div>
</form>

