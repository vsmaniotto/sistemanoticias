@extends('layouts.admin')

@section('titulo', 'Área administrativa')

@section('conteudo')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>Editar Categorias</h2>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-12">
                @include('admin.categorias.form')
            </div>
        </div>
    </div>
@endsection
    