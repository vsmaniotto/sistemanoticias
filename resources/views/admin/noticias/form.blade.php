{{-- Cadastros São method="POST" --}}
<form action="#" method="POST"> 
    <div class="form-group row">
        <label for="titulo" class="col-sm-2 col-form-label text-right font-weight-bold">Título</label>
        <div class="col-sm-10">        
            <input type="text" name="titulo" id="titulo" value="" class="form-control">
        </div>
    </div>
    <div class="form-group row">
        <label for="subtitulo" class="col-sm-2 col-form-label text-right font-weight-bold">Subtítulo</label>
        <div class="col-sm-10">        
            <input type="text" name="subtitulo" id="subtitulo" value="" class="form-control">
        </div>
    </div>
    <div class="form-group row">
        <label for="descricao" class="col-sm-2 col-form-label text-right font-weight-bold">Descrição</label>
        <div class="col-sm-10">        
            <textarea type="text" name="descricao" id="descricao" value="" rows="5" class="form-control"></textarea>
        </div>
    </div>
    <div class="form-group row">
        <label for="status" class="col-sm-2 col-form-label text-right font-weight-bold">Status</label>
        <div class="col-sm-3">
            <select name="status" id="status" class="form-control">
                <option value="0">Não Publicado</option>
                <option value="1">Aguardando Revisão</option>
                <option value="2">Publicado</option>
                <option value="3"></option>
            </select>
        </div>
    </div>
    <div class="form-group row">
        <div class="offset-sm-2 col-sm-10">
            <button type="submit" class="btn btn-danger">Enviar</button>
            <a href="#" class="btn btn-secondary">Cancelar</a>
        </div>
    </div>
</form>