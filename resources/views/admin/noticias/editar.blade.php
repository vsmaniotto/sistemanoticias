@extends('layouts.admin')

@section('titulo', 'Área administrativa')

@section('conteudo')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>Editar Notícia</h2>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-12">
                @include('admin.noticias.form')
            </div>
        </div>
    </div>
@endsection
    