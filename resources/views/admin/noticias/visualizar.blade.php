@extends('layouts.admin')

@section('titulo', 'Área administrativa')

@section('conteudo')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>Visualizar Notícia</h2>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-12">
               <table class="table table-striped table-condensed">
                    <tr>
                        <th width="150">ID</th>
                        <td>1</td>
                    </tr>
                    <tr>
                        <th width="150">Título</th>
                        <td>Lorem, ipsum dolor sit amet consectetur adipisicing elit.</td>
                    </tr>
                    <tr>
                        <th width="150">Subtítulo</th>
                        <td>Lorem, ipsum dolor sit amet consectetur adipisicing elit.</td>
                    </tr>
                    <tr>
                        <th width="150">Descrição</th>
                        <td>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quidem ut autem aliquid totam quibusdam. Reiciendis, corrupti assumenda deserunt, a nobis qui at, asperiores sequi neque autem quaerat provident eveniet eligendi. </td>
                    </tr>
                    <tr>
                        <th width="150">Status</th>
                        <td>Não Publicado</td>
                    </tr>
               </table>
               <a href="#" class="btn btn-danger">Editar Notícia</a>
               <a href="#" class="btn btn-secondary">Cancelar</a>
            </div>
        </div>
    </div>
@endsection
    